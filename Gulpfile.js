var gulp	 		  = require('gulp'),
    path			  = require('path'),
    jspm 				= require('jspm'),
    rename		  = require('gulp-rename'),
    uglify	 		= require('gulp-uglify'),
    htmlreplace = require('gulp-html-replace'),
    ngAnnotate  = require('gulp-ng-annotate'),
    sass        = require('gulp-sass'),
    serve			  = require('browser-sync'),
    yargs			  = require('yargs').argv;

var root = 'client';

// helper method to resolveToApp paths
var resolveTo = function(resolvePath) {
  return function(glob) {
    glob = glob || '';
    return path.join(root, resolvePath, glob);
  }
};

var resolveToApp = resolveTo('app'); // app/{glob}

// map of all our paths
var paths = {
  js: resolveToApp('**/*.js'),
  css: resolveToApp('**/*.css'),
  html: [
    resolveToApp('**/*.html'),
    path.join(root, 'index.html')
  ],
  dist: path.join(__dirname, 'dist/')
};

gulp.task('sass', function () {
  gulp.src(['./client/app/**/*.scss'], { base: './client/app' })
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./client/app'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./client/app/**/*.scss', ['sass']);
});

gulp.task('serve', ['sass', 'sass:watch'], function() {
  serve({
    port: process.env.PORT || 3000,
    open: true,
    files: [].concat(
      [paths.js],
      [paths.css],
      paths.html
    ),
    server: {
      baseDir: root,
      // serve our jspm dependencies with the client folder
      routes: {
        '/jspm.config.js': './config.js',
        '/jspm_packages': './jspm_packages'
      }
    }
  });
});

gulp.task('build', function() {
  var dist = path.join(paths.dist + 'app.js');
  // Use JSPM to bundle our app
  return jspm.bundleSFX(resolveToApp('app'), dist, {})
    .then(function() {
      // Also create a fully annotated minified copy
      return gulp.src(dist)
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(rename('app.min.js'))
        .pipe(gulp.dest(paths.dist))
    })
    .then(function() {
      // Inject minified script into index
      return gulp.src('client/index.html')
        .pipe(htmlreplace({
          'js': 'app.min.js'
        }))
        .pipe(gulp.dest(paths.dist));
    });
});

gulp.task('default', ['serve']);