import angular from 'angular';
import Github from './github/github';
import Loading from './loading/loading';

let commonModule = angular.module('searchHubApp.common', [
	Github.name,
	Loading.name
]);

export default commonModule;