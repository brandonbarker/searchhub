import angular from 'angular';
import loadingComponent from './loading.component';

let loadingModule = angular.module('searchHubApp.loading', [])
  .directive('loading', loadingComponent);

export default loadingModule;