let loadingComponent = () => {
  return {
    restrict: 'A',
    scope: false,
    link: (scope, element, attrs) => {
      element.append('<div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div>');

      scope.$watch(attrs.loading,
        function (isLoading) {
          if (isLoading === true) {
            element.addClass('loading');
          } else {
            element.removeClass('loading');
          }
        });
    }
  };
};

export default loadingComponent;