import angular from 'angular';
import GithubService from './github.service';

let githubModule = angular.module('searchHubApp.github', [])
  .service('GithubSvc', GithubService);

export default githubModule;