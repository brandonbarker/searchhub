const GithubApiUrl = 'https://api.github.com';

/*
 Description	                                      URL
 Sample API URL to search by repository name	      https://api.github.com/search/repositories?q=bootstrap
 API URL to display issues of a repository name	    https://api.github.com/search/issues?q=repo:username/reponame
 Example: Display Issues of Bootstrap	              https://api.github.com/search/issues?q=repo:twbs/bootstrap
 API Documentation	                                https://developer.github.com/v3/search/#search-issues
 GItHub Search API Documentation	                  https://developer.github.com/v3/search/
 */

class GithubService {
  /** @ngInject */
  constructor($http, $q) {
    this.$http = $http;
    this.$q = $q;
    this.SearchMap = new Map();
    this.ReadmeMap = new Map();
  }

  listRepos(search) {
    // Search our Search cache first and return results if found
    if (this.SearchMap.has(search)) {
      let deferred = this.$q.defer();

      deferred.resolve(this.SearchMap.get(search));

      return deferred.promise;
    } else {
      console.log(`Searching for '${search}'`);

      return this.$http({
        url: `${GithubApiUrl}/search/repositories?q=${search}`,
        method: 'GET'
      }).then((result) => {
        // Cache our results
        this.SearchMap.set(search, result.data);

        return result.data;
      })
    }
  }

  getReadme(repo) {
    let decodedRepo = decodeURIComponent(repo);

    if (this.ReadmeMap.has(decodedRepo)) {
      let deferred = this.$q.defer();

      deferred.resolve(this.ReadmeMap.get(decodedRepo));

      return deferred.promise;
    } else {
      let url = `${GithubApiUrl}/repos/${repo}/readme`;
      console.log(`Fetching README for '${decodedRepo}'`);

      return this.$http({
        url: decodeURIComponent(url),
        method: 'GET',
        headers: {
          'Accept': 'application/vnd.github.VERSION.raw'
        }
      }).then((result) => {
        this.ReadmeMap.set(decodedRepo, result.data);

        return result.data;
      })
    }
  }

  getRepo(repo) {
    let decodedRepo = decodeURIComponent(repo);
    let url = `${GithubApiUrl}/repos/${decodedRepo}`;

    return this.$http({
      url,
      method: 'GET'
    }).then((result) => {
      return result.data;
    })
  }

  getRepoIssues(repo) {
    let decodedRepo = decodeURIComponent(repo);
    let url = `${GithubApiUrl}/repos/${decodedRepo}/issues`;

    return this.$http({
      url,
      method: 'GET'
    }).then((result) => {
      return result.data;
    })
  }

  getRepoStats(repo) {
    let decodedRepo = decodeURIComponent(repo);
    let url = `${GithubApiUrl}/repos/${decodedRepo}/stats/participation`;

    return this.$http({
      url,
      method: 'GET'
    }).then((result) => {
      return result.data;
    })
  }
}

export default GithubService