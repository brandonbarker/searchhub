import angular from 'angular';
import 'angular-ui-router';
import 'angular-animate';
import Common from './common/common';
import Components from './components/components';
import 'normalize.css';
import './app.css!';
import 'daneden/animate.css';

let appModule = angular.module('searchHubApp', [
  'ui.router',
  'ngAnimate',
  Common.name,
  Components.name
]);

/*
 * As we are using ES6 with Angular 1.x we can't use ng-app directive
 * to bootstrap the application as modules are loaded asynchronously.
 * Instead, we need to bootstrap the application manually
 */

angular.element(document).ready(() => {
  angular.bootstrap(document, [appModule.name]), {
    strictDi: true
  }
});

export default appModule;