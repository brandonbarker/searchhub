import controller from './search.controller';
import template from './search.html!text';
import './search.css!';

let searchComponent = () => {
  return {
    template,
    controller,
    restrict: 'E',
    controllerAs: 'search',
    bindToController: true
  };
};

export default searchComponent;