import angular from 'angular';
import 'angular-ui-router';
import searchComponent from './search.component';

let searchModule = angular.module('searchHubApp.search', [
  'ui.router'
])
.config(($stateProvider) => {
  $stateProvider
    .state('search', {
      url: '/search/{searchTerm}',
      template: '<search></search>'
    });
})
.directive('search', searchComponent);

export default searchModule;