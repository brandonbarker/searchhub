class SearchController {
  /** @ngInject */
  constructor($stateParams, $state, $rootScope, GithubSvc) {
    this.$state = $state;
    this.$rootScope = $rootScope;
    this.github = GithubSvc;

    console.log('State: ', $state.current);

    if ($stateParams.searchTerm) {
      this.findRepos($stateParams.searchTerm);
    }
  }

  findRepos(repoName) {
    if (repoName) {
      this.loading = true;

      this.github.listRepos(repoName).then((repos) => {
        this.loading = false;
        this.repos = repos.items;
        this.count = repos.total_count;
      });
    }
  }
}

export default SearchController;