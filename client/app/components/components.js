import angular from 'angular';
import Details from './details/details';
import Home from './home/home';
import Search from './search/search';
import Navbar from './navbar/navbar';

let componentModule = angular.module('searchHubApp.components', [
	Details.name,
	Home.name,
	Search.name,
	Navbar.name
]);

export default componentModule;