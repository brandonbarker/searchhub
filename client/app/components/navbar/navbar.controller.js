class NavbarController {
  /** @ngInject */
  constructor($state) {
    this.$state = $state;
  }

  search() {
    if (this.repoName) {
      this.$state.go('search', { searchTerm: this.repoName });
    }
  }
}

export default NavbarController;