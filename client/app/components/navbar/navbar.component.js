import controller from './navbar.controller';
import template from './navbar.html!text';

let navbarComponent = () => {
  return {
    template,
    controller,
    restrict: 'E',
    controllerAs: 'nav',
    bindToController: true
  };
};

export default navbarComponent;