import angular from 'angular';
import './navbar.css!';
import navbarComponent from './navbar.component';

let navbarModule = angular.module('searchHubApp.navbar', [])
  .directive('navbar', navbarComponent);

export default navbarModule;