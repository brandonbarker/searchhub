import angular from 'angular';
import 'angular-ui-router';
import 'angular-sanitize';
import template from './details.html!text';
import controller from './details.controller';
import readmeTemplate from './partials/readme.html!text';
import issuesTemplate from './partials/issues.html!text';
import statsTemplate from './partials/stats.html!text';
import './details.css!';
import 'marked';
import 'Hypercubed/angular-marked';
import 'urish/angular-moment';
import 'chart.js';
import 'jtblin/angular-chart.js';

let detailsModule = angular.module('searchHubApp.details', [
  'ui.router',
  'hc.marked',
  'angularMoment',
  'chart.js'
])
.config(($stateProvider) => {
  $stateProvider
    .state('search.details', {
      url: '/details/{repo}',
      template,
      controller,
      controllerAs: 'details',
      bindToController: true
    })

    .state('search.details.readme', {
      url: '/readme',
      template: readmeTemplate
    })

    .state('search.details.issues', {
      url: '/issues',
      template: issuesTemplate
    })

    .state('search.details.stats', {
      url: '/stats',
      template: statsTemplate
    });
});

export default detailsModule;