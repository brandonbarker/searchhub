class DetailsController {
  /** @ngInject */
  constructor($stateParams, $rootScope, GithubSvc) {
    this.$rootScope = $rootScope;
    this.github = GithubSvc;

    let repoName = $stateParams.repo;

    if (repoName) {
      this.getRepoDetails(repoName);
      this.getReadme(repoName);
      this.getRepoStats(repoName);
    }
  }

  getRepoDetails(repoName) {
    this.loading = true;
    this.github.getRepo(repoName).then((repo) => {
      this.loading = false;
      this.repo = repo;
    });

    this.github.getRepoIssues(repoName).then((issues) => {
      this.issues = issues;
    });
  }

  getReadme(repo) {
    this.loading = true;
    this.github.getReadme(repo).then((readme) => {
      this.loading = false;

      if (readme) {
        this.readme = readme;
      }
    }, (error) => {
      this.loading = false;
      this.readmeNotFound = true;
    });
  }

  getRepoStats(repoName) {
    this.loading = true;
    this.github.getRepoStats(repoName).then((stats) => {
      this.loading = false;
      this.stats = stats;

      this.chartData = [ this.stats.all, this.stats.owner ];
      this.chartSeries = [ 'All Contributions', 'Owner Contributions' ];
      this.chartLabels = this.generateWeekNumbers();
    });
  }

  generateWeekNumbers() {
    let weeks = [];

    for (var index = 1; index < 53; index++) {
      weeks.push(`Week ${index}`);
    }

    return weeks;
  }
}

export default DetailsController;