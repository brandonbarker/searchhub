import angular from 'angular';
import 'angular-ui-router';
import template from './home.html!text';
import './home.css!';

let homeModule = angular.module('home', [
	'ui.router'
])
.config(($stateProvider, $urlRouterProvider) => {
	$urlRouterProvider.otherwise('/');
	
	$stateProvider
		.state('home', {
			url: '/',
			template
		});
});

export default homeModule;