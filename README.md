# SearchHub #

An ES6/Angular 1.4.x application that will allow a user to search a repository by name and display a repository's relevant information such as URL, description, forks count, stargazers count, open issues count etc.

## Installation

* Clone the repo
* cd into the project directory
* Run `npm install`
* Run `jspm install`
* Run `gulp serve`

## Tech Stack

* ES6 using JSPM
* Angular 1.4.x
* Bootstrap
* Gulp